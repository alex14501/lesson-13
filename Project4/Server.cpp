#include "Server.h"
#include <exception>
#include <iostream>
#include <string>
#include <mutex>
#include <set>
#include "Helper.h"
#include <map>
#include <fstream>
#include <iostream>
#define CONNECT "200"
#define UPDATE "204"
#define FINISH "207"
#define DISCONNECT "208"
#define CONNECT_NUM 1
#define UPDATE_NUM 2
#define FINISH_NUM 3
#define DISCONNECT_NUM 4
#define DELETE 1
#define CHANGE 0
#define NAME_LENGTH_START 3
#define NAME_LENGTH_END 5
#define DATA_LENGTH_START 3
#define DATA_LENGTH_END 8
#define LAST_PLACE 1
#define END_CODE 3
#define MAX_LENGTH 99999
std::mutex updateLock;
std::mutex userSocketsLock;
std::mutex usernamesLock;
Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;
	this->_content.clear();
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();//thread
	}
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the function that handle the conversation with the client
	std::thread t(&Server::clientHandler,this,client_socket);//creating a new thread for each client that connects
	t.detach();
}

//this function handels a client(client thread)
void Server::clientHandler(SOCKET clientSocket)
{
	int functionNum = 0;
	int i = 0;
	std::string function;
	std::string name;
	std::string data;
	int length = 0;
	std::string strLength;
	std::pair<SOCKET, int> pair;
	name.clear();
	data.clear();
	char m[MAX_LENGTH];
	while (functionNum != DISCONNECT_NUM)
	{
		//reset
		length = 0;
		strLength.clear();
		data.clear();
		recv(clientSocket, m, MAX_LENGTH, 0);//getting message from the client
		functionNum = getFunctionNum(m);
		switch (functionNum)
		{
		case CONNECT_NUM:
			strLength = this->getString(m, NAME_LENGTH_START, NAME_LENGTH_END);
			length = atoi(strLength.c_str());
			length += NAME_LENGTH_END;//getting name end pos in the string
			name = this->getString(m, NAME_LENGTH_END, length);
			userSocketsLock.lock();
			usernamesLock.lock();
			if (this->_users.empty())//checking if thats a completly new user than i have to load from the data.txt
			{
				this->_content = this->readFile();
			}
			this->_users.push(name);//entering the name into the system
			pair = std::make_pair(clientSocket, this->_users.size());
			this->_sockets.insert(pair);
			Helper::sendUpdateMessageToClient(clientSocket, this->_content, this->_users.front(), this->getNext(), this->_users.size());//sending 101 to the connected client
			userSocketsLock.unlock();
			usernamesLock.unlock();
			break;
		case UPDATE_NUM:
			strLength = this->getString(m, DATA_LENGTH_START, DATA_LENGTH_END);
			length = atoi(strLength.c_str());
			length += DATA_LENGTH_END;//data section end pos
			data = this->getString(m, DATA_LENGTH_END, length);
			this->_content = data;//setting new data
			updateLock.lock();
			this->update();
			updateLock.unlock();
			break;
		case FINISH_NUM:
			usernamesLock.lock();
			this->_users.push(this->_users.front());//putting the name last in the queue
			this->_users.pop();//poping it out from the first pos
			this->changePostions(CHANGE);//changing current positions of all users
			usernamesLock.unlock();
			updateLock.lock();
			this->update();
			updateLock.unlock();
			break;
		case DISCONNECT_NUM:
			userSocketsLock.lock();
			usernamesLock.lock();
			this->erase(name);//erasing the name from the client list
			this->_sockets.erase(clientSocket);//erasing the socket from the socket list
			this->changePostions(DELETE);//changing all positions and delete current pos
			userSocketsLock.unlock();
			usernamesLock.unlock();
			updateLock.lock();
			this->update();
			updateLock.unlock();
			break;
		default:
			break;
		}
	}
	closesocket(clientSocket);//closing the socket
}
//this function extractes a string within another string dimensions
std::string Server::getString(char* m, int start,int end)
{
	std::string Message;
	for (int i = start; i < end; i++)
	{
		Message += m[i];
	}
	return Message;
}
//this function seaches for a name and deletes the name
void Server::erase(std::string name)
{
	std::queue<std::string> tempNames;
	std::string checkName;
	while (!this->_users.empty())
	{
		checkName = this->_users.front();
		this->_users.pop();
		if (name != checkName)//if the name is found, dint push it back(basicly throwing it away)
		{
			tempNames.push(checkName);
		}
	}
	while (!tempNames.empty())//retriving the queue
	{
		this->_users.push(tempNames.front());
		tempNames.pop();
	}
}
//this function can delete a client and update all other client position(DELETE) or, this function can make every client step in his pos(CHANGE)
void  Server::changePostions(int func)
{
	std::map<SOCKET, int>::iterator iter;
	int prev = 0;
	for (iter = this->_sockets.begin(); iter != this->_sockets.end(); iter++)
	{
		if (func == CHANGE)//check if there is a need to step in pos or delete a user and change all other positions
		{
			if ((*iter).second - LAST_PLACE == 0)
			{
				(*iter).second = this->_sockets.size();
			}
			else
			{
				(*iter).second--;
			}
		}
		else
		{
			if (prev != 0 && (*iter).second > prev + 1)//checking if now client pos=1,and if a client was delete than there is a step of 2 created by the deleted client instead of one
			{
				(*iter).second -= 1;
			}
			prev = (*iter).second;
		}
	}
	
}
//this function retrives the user with pos num=2
std::string Server::getNext()
{
	std::string name;
	std::queue<std::string> tempNames;
	if (!this->_users.empty())//check if there is any clients connected
	{
		name = this->_users.front();//save name in case there is only one user
		this->_users.pop();//throwing the first pos from the queue//second pos should be left
		tempNames.push(name);//put the name back
		if (!this->_users.empty())//checks if only one client exists
		{
			name = this->_users.front();//save name
			this->_users.pop();
			tempNames.push(name);//put the name back
			while (!this->_users.empty())//throwing all users out to retrive them afterwards toghther
			{
				tempNames.push(this->_users.front());
				this->_users.pop();
			}
			while (!tempNames.empty())//retrining users back to the queue
			{
				this->_users.push(tempNames.front());
				tempNames.pop();
			}
		}
		else
		{//in case there is only one user, put it back
			this->_users.push(tempNames.front());
			tempNames.pop();
		}
	}
	return name;
}
//this function reads the data file
std::string Server::readFile()
{
	std::ifstream ifs("data.txt");
	std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();
	return str;
}
//this function updates the data.txt file with current content that is being displayed at the magshidocs
void Server::updateFile()
{
	std::ofstream file;
	std::ofstream ofs;
	ofs.open("data.txt", std::ofstream::out | std::ofstream::trunc);//cleaning the file
	ofs.close();
	file.open("data.txt");//writing the new content to the file
	file << this->_content;
	file.close();
}
//this function update all the clients with the current data,pos,curr user
void Server::update()
{
	userSocketsLock.lock();
	usernamesLock.lock();
	std::map<SOCKET,int>::iterator iter;
	std::queue<std::string> tempNames;
	for (iter = this->_sockets.begin(); iter != this->_sockets.end(); iter++)//sending message to every user that is connected right now
	{
		Helper::sendUpdateMessageToClient((*iter).first,this->_content, this->_users.front(), this->getNext(), (*iter).second);
	}
	this->updateFile();//update data content
	usernamesLock.unlock();
	userSocketsLock.unlock();
}
//this function return the function num for the clientHandler ***case**** like "200" is 1...
int Server::getFunctionNum(char* function)
{
	std::string functionStr;
	for (int i = 0; i < END_CODE; i++)
	{
		functionStr += function[i];
	}
	if (functionStr == CONNECT)
	{
		return CONNECT_NUM;
	}
	else
	{
		if (functionStr == UPDATE)
		{
			return UPDATE_NUM;
		}
		else
		{
			if (functionStr == FINISH)
			{
				return FINISH_NUM;
			}
			else
			{
				if (functionStr == DISCONNECT)
				{
					return DISCONNECT_NUM;
				}
			}
		}
	}
	return 0;//in any case that there is some kind of an error
}