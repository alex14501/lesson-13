#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <queue>
#include <string>
#include <set>
#include <map>
class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:

	void accept();
	void clientHandler(SOCKET clientSocket);
	std::queue<std::string> _users;
	void changePostions(int func);
	SOCKET _serverSocket;
	int getFunctionNum(char* function);
	void update();
	void erase(std::string name);
	std::string _content;
	std::string getNext();
	std::map<SOCKET, int> _sockets;
	void updateFile();
	std::string readFile();
	std::string getString(char* m, int start,int end);
};
